package com.example.simpatiser;

import android.os.Bundle;
import android.telephony.SmsMessage;
import android.telephony.gsm.SmsManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.lang.reflect.Method;
import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class MainActivity extends AppCompatActivity {
    private String logTag = "SIMpatiser";

    // from https://stackoverflow.com/questions/10339504/accessing-sms-messages-on-sim
    public ArrayList<SmsMessage> getSimCardMessages() {

        // this implementation was recorded to work for android 1.6
        // it was tested Ok on android 2.3

        ArrayList<SmsMessage> list = new ArrayList<SmsMessage>();

        try {
            Class<?> smsMgrClass = Class.forName("android.telephony.SmsManager");
            Method getSMSMgr = smsMgrClass.getMethod("getDefault");
            Object smsDefaultInstance = getSMSMgr.invoke(null);
            Method getMessages = smsMgrClass.getMethod("getAllMessagesFromIcc");
            //@SuppressWarnings("unchecked")
            list = (ArrayList<SmsMessage>) getMessages.invoke(smsDefaultInstance);

            Log.d(logTag, "length: " + list.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    protected String notNullOrDefault(String s, String def) {
        return s == null ? def : s;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        ArrayList<SmsMessage> list = getSimCardMessages();
        //Toast.makeText(context, "# messages on SIM: " + list.count()).show();

        int notInSIM = 0;
        for(SmsMessage message : list) {
            String from = "" + message.getOriginatingAddress();
            String body = "" + message.getMessageBody();
            boolean inSIM = (message.getIndexOnSim() != -1);
            if (inSIM) {
                byte[] pdu = message.getPdu();
                StringBuilder pduString = new StringBuilder();
                for (byte b : pdu) {
                    // from https://stackoverflow.com/questions/40907601/converting-byte-to-hex-string-in-android
                    pduString.append(String.format("%02x", b & 0xff));
                }

                String csv = ""
                           + ",,," + notNullOrDefault(message.getDisplayMessageBody(), "")
                           + ",,," + notNullOrDefault(message.getMessageBody(), "")

                           + ",,," + notNullOrDefault(message.getDisplayOriginatingAddress(), "")
                           + ",,," + notNullOrDefault(message.getOriginatingAddress(), "")

                           + ",,," + message.getIndexOnIcc()
                           + ",,," + message.getStatusOnIcc()

                           + ",,," + message.getTimestampMillis()

                           + ",,," + notNullOrDefault(message.getServiceCenterAddress(), "")
                           + ",,," + message.getProtocolIdentifier()
                           + ",,," + pduString.toString();

                Log.d(logTag, "sms on sim = " + inSIM + ":" + csv);

                int status = message.getStatusOnSim();
//                if (status == SmsManager.STATUS_ON_SIM_UNREAD) {
//                    Toast.makeText(context, "Message Unread on SIM: " + from + ": " + body, Toast.LENGTH_SHORT).show();
//                }
            } else {
                notInSIM += 1;
            }
        }
        Log.d(logTag, "Not on sim: " + notInSIM);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}